


1.1. getLastestArticlesOfUser

requset :
        {
            "method": "getLastestArticlesOfUser",
            "user_id":"1",
        }

response
        {
            "result":[],
            "reason":""
        }


1.2 getLastestArticles

requset :
        {
            "method": "getArticlesByUserId",
            "limit": limit,
            "offset": 1,
            "currentTime" : "unixtime" // optional

        }

response
        {
            "result":[],
            "error":""
        }


1.3. addArticle
request

        {
            "method": "addArticle",
            "user_id": "xx",
            "title": "xx",
            "content": "something"
            "pubtime": 2018-01-26 13:13:45

        }

response
        {
            "error":""
        }

1.4 deleteArticleById

request

        {
            "method": "deleteArticleById",
            "id": "xx",
        }

response
        {
            "error":""
        }

1.5 modifyArticleById

request

        {
            "method": "modifyArticleById",
            "id": "xx",
            "title":"Tea",
            "content":"something",
        }

response
        {
            "error":""
        }

1.6 setArticleBanned

       request

                {
                    "method": "setArticleBanned",
                    "id": "xx"  // article id
                }

       response
                {
                    "error":""
                }

1.7 searchArticles

requset :
        {
            "method": "searchArticles",
            "limit" : limit,
            "offset" : offset,
            "rules": [],  //
            "currentTime" : currentTime
        }

        rules :  [ {"field": "title",
                    "asc": true,
                     "value1": "xx",
                      "value2": null},

                    {"field": "pubtime",
                    "asc": true,
                     "value1": "xx",
                      "value2": "xx"},

                    {"field": "userName",
                    "asc": true,
                     "value1": "xx",
                      "value2": null},
                       ]

response
        {
            "result":[],
            "error":""
        }


2 Comment

2.1 getCommentTreeByArticleId

       request

                {
                    "method": "getCommentTreeByArticleId",
                    "article_id": "xx"
                }

       response
                {
                    "result":[],  //list of Comment
                    "error":""
                }

2.2 addComment

       request

                {
                    "method": "addComment",
                    "user_id": "xx",
                    "article_id": "xx",
                    "parent_id": "xx",
                    "content": "something"

                }

       response
                {
                    "error":""
                }

2.3 deleteCommentById

       request

                {
                    "method": "deleteCommentById",
                    "id": "xx",
                }

       response
                {
                    "error":""
                }


2.4 getLatestComments
       request

                {
                    "method": "getLatestComments",
                    "limit": "xx"
                    "offset": "xx"
                }

       response
                {
                    "result":[],  //list of Comment
                    "error":""
                }


3.5 setCommentBanned

       request

                {
                    "method": "setCommentBanned",
                    "id": "xx"  // comment id
                }

       response
                {
                    "error":""
                }


3. User

3.1 getAllUsers

       request

                {
                    "method": "getAllUsers",
                    "limit": "xx",
                    "offset" : "0"
                }

       response
                {
                    "result":[],  //list of User
                    "error":""
                }


3.2 getUserById

       request

                {
                    "method": "getUserById",
                    "id": "xx",
                }

       response
                {
                    "result": user,  // User
                    "error":""
                }


3.2 deleteUserById

       request

                {
                    "method": "deleteUserById",
                    "id": "xx",
                }

       response
                {
                    "error":""
                }


3.2 modifyUser

       request

                {
                    "method": "modifyUser",
                    "id": "xx",
                    "fname": "xx",
                    "lname": "xx",
                    "passwd": "xx",
                    "email": "xx",
                    "gender": "xx",
                    "icon": "xx",
                    "dob": "xx",
                    "country": "xx",
                    "description": "xx",

                }

       response
                {
                    "error":""
                }


3.2 addUser

       request

                {
                    "method": "addUser",
                    "id": "xx",
                    "username":"xxx",
                    "fname": "xx",
                    "lname": "xx",
                    "passwd": "xx",
                    "email": "xx",
                    "gender": "xx",
                    "icon": "xx",
                    "dob": "xx",
                    "country": "xx",
                    "description": "xx",

                }

       response
                {
                    "error":""
                }

3.3 getUserByUserName
       request

                {
                    "method": "getUserByUserName",
                    "userName": "xx",
                }

       response
                {
                    "result": user,  // User
                    "error":""
                }


