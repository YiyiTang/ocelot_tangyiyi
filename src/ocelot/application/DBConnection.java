package ocelot.application;

import ocelot.application.beans.Article;
import ocelot.application.beans.Comment;
import ocelot.application.beans.User;
import org.jooq.*;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import static ocelot.generated.Tables.*;
import static org.jooq.impl.DSL.row;

public class DBConnection implements AutoCloseable{

    private Connection con_ = null;
    private DSLContext create_ = null;

    static {
        try {
            //TimeZone timeZone = TimeZone.getTimeZone("+13:00");
            //TimeZone.setDefault(timeZone);
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private DBConnection(Connection con, DSLContext create) {
        this.con_ = con;
        this.create_ = create;
    }

    public static DBConnection create(Properties dbProps) throws SQLException {

        Connection con = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
        DSLContext create = DSL.using(con, SQLDialect.MYSQL);

        return new DBConnection(con, create);
    }

    public void clearDB() {
        create_.truncate(COMMENT).execute();
        create_.truncate(ARTICLE).execute();
        create_.truncate(USER).execute();
    }

    public List<User> getAllUsers(int limit, int offset){
        List<User> list = new ArrayList<>();

        Result<Record> result = create_.select()
                .from(USER).orderBy(USER.USERNAME.asc())
                .limit(limit)
                .offset(offset)
                .fetch();
        for (Record r : result) {
            list.add(record2User(r));
        }

        return list;
    }

    public User getUserById(String userId) {

        Result<Record> result = create_.select()
                .from(USER)
                .where(USER.ID.eq(Integer.valueOf(userId)))
                .fetch();

        if (result.size() > 0 ) {
            Record r = result.get(0);
            return record2User(r);
        }

        return null;
    }


    /**
     * search users by any name (first / last/ username)
     * @param name pattern
     * @return
     */
    public List<User> getUsersByNamePattern(String name) {

        List<User> list = new ArrayList<>();

        if (name.isEmpty()) {
            return list;
        }

        String pattern = "%" + name + "%";

        Result<Record> result = create_.select().from(USER)
                .where(USER.USERNAME.like(pattern))
                .or(USER.FNAME.like(pattern)).or(USER.LNAME.like(pattern))
                .fetch();

        for (Record r : result) {
            list.add(record2User(r));
        }

        return list;
    }

    public User getUserByUserName(String userName) {

        Result<Record> result = create_.select()
                .from(USER)
                .where(USER.USERNAME.eq(userName))
                .fetch();

        if (result.size() > 0 ) {
            Record r = result.get(0);
            return record2User(r);
        }

        return null;
    }



    public void addUser(User user) {

        create_.insertInto(USER,
                USER.USERNAME,
                USER.FNAME,
                USER.LNAME,
                USER.PASSWD,
                USER.GENDER,
                USER.EMAIL,
                USER.ICON,
                USER.DOB,
                USER.COUNTRY,
                USER.DESCRIPTION)
                .values(user.getUserName(),
                        user.getFname(),
                        user.getLname(),
                        user.getPasswd(),
                        user.getGender(),
                        user.getEmail(),
                        user.getIconName(),
                        Date.valueOf(user.getDob()),
                        user.getCountry(),
                        user.getDescription()).execute();

    }

    public void modifyUser(User user) {

        create_.update(USER)
                .set(USER.FNAME, user.getFname())
                .set(USER.LNAME, user.getLname())
                .set(USER.PASSWD, user.getPasswd())
                .set(USER.EMAIL, user.getEmail())
                .set(USER.GENDER, user.getGender())
                .set(USER.ICON, user.getIconName())
                .set(USER.DOB, Date.valueOf(user.getDob()))
                .set(USER.COUNTRY, user.getCountry())
                .set(USER.DESCRIPTION, user.getDescription())
                .where(USER.ID.eq(Integer.valueOf(user.getId())))
                .execute();

    }


    public void changeUserPwd(String userId, String pwd) {

        create_.update(USER)
                .set(USER.PASSWD, pwd)
                .where(USER.ID.eq(Integer.parseInt(userId)))
                .execute();

    }

    public void verifyUser(String userId, boolean verified) {
        create_.update(USER)
                .set(USER.VERIFIED, verified ? 1: 0)
                .where(USER.ID.eq(Integer.valueOf(userId)))
                .execute();
    }

    public void deleteUserById(String userId) {

        create_.delete(USER)
                .where(USER.ID.eq(Integer.valueOf(userId)))
                .execute();
    }

    public void deleteUserByUserName(String username) {

        User user = getUserByUserName(username);

        if (user != null) {
            deleteUserById(user.getId());
        }

    }


    private User record2User(Record r) {
        String id = "" + r.getValue(USER.ID);
        String userName = r.getValue(USER.USERNAME);
        String fname = r.getValue(USER.FNAME);
        String lname = r.getValue(USER.LNAME);
        String passwd = r.getValue(USER.PASSWD);
        String gender = r.getValue(USER.GENDER);
        String email = r.getValue(USER.EMAIL);
        String icon = r.getValue(USER.ICON);
        boolean verified = r.getValue(USER.VERIFIED) > 0;

        User user = new User(id, userName, fname, lname, passwd, gender, email, icon, verified);

        user.setDob(r.getValue(USER.DOB).toString());
        user.setCountry(r.getValue(USER.COUNTRY));
        user.setDescription(r.getValue(USER.DESCRIPTION));

        return user;
    }


    public List<Article> getLastestArticles(int limit, int offset, boolean filterBanned, Timestamp currentTime) {

            //TestSearch();

        List<Article> list = new ArrayList<>();
        Result<Record> result = null;
        if (filterBanned) {

            Condition condition = ARTICLE.BANNED.eq(0);
            if (currentTime != null) {
                condition = condition.and(ARTICLE.PUBTIME.le(currentTime));
            }
            result = create_.select()
                    .from(ARTICLE)
                    .join(USER).onKey()
                    .where(condition)
                    .orderBy(ARTICLE.PUBTIME.desc())
                    .limit(limit).offset(offset)
                    .fetch();
        } else {

            if (currentTime != null) {
                result = create_.select()
                        .from(ARTICLE)
                        .join(USER).onKey()
                        .where(ARTICLE.PUBTIME.le(currentTime))
                        .orderBy(ARTICLE.PUBTIME.desc())
                        .limit(limit).offset(offset)
                        .fetch();
            } else {
                result = create_.select()
                        .from(ARTICLE)
                        .join(USER).onKey()
                        .orderBy(ARTICLE.PUBTIME.desc())
                        .limit(limit).offset(offset)
                        .fetch();
            }
        }

        for (Record r : result) {
            list.add(record2Article(r));
        }

        return list;
    }

    public List<Article> getHotestArticles(int limit, int offset) {

        List<Article> list = new ArrayList<>();

        Result<Record> result = create_.select()
                .from(ARTICLE)
                .join(USER).onKey()
                .orderBy(ARTICLE.HITCOUNT.desc())
                .limit(limit).offset(offset)
                .fetch();
        for (Record r : result) {
            list.add(record2Article(r));
        }

        return list;
    }

    public List<Article> getAllArticlesOfUser(String userid, boolean filterBanned) {
        List<Article> list = new ArrayList<>();

        Condition condition = ARTICLE.USER_ID.eq(Integer.valueOf(userid));
        if (filterBanned) {
            condition = condition.and(ARTICLE.BANNED.eq(0));
        }

        Result<Record> result = create_.select()
                .from(ARTICLE)
                .join(USER).onKey()
                .where(condition)
                .fetch();
        for (Record r : result) {
            list.add(record2Article(r));
        }

        return list;
    }


    private Condition toCondition(String field, Object val1, Object val2) {
        switch (field) {
            case "title" : {
                String value = (String) val1;
                return ARTICLE.TITLE.like("%" + value + "%");
            }
            case "pubtime" : {
                Timestamp value1 = (Timestamp) val1;
                Timestamp value2 = (Timestamp) val2;
                return ARTICLE.PUBTIME.between(value1, value2);
            }

            case "userName" :
                return USER.USERNAME.eq(((String)val1));
        }

        return null;
    }

    private SortField<?> toSortField(String field, boolean asc) {

        switch (field) {
            case "title" :
                return asc ? ARTICLE.TITLE.asc() : ARTICLE.TITLE.desc();

            case "pubtime" :
                return asc ? ARTICLE.PUBTIME.asc() : ARTICLE.PUBTIME.desc();
        }

        return null;
    }


    private void TestSearch() {
        JSONArray array = new JSONArray();
        JSONObject titleSearch = new JSONObject();


        titleSearch.put("field", "title");
        titleSearch.put("value1", "Article");
        titleSearch.put("asc", false);
        //array.add(titleSearch);

        JSONObject timeSearch = new JSONObject();
        timeSearch.put("field", "pubtime");
        timeSearch.put("asc", true);
        timeSearch.put("value1", 1517230400001L);
        timeSearch.put("value2", 1517629467000L);
        //timeSearch.put("value1", 630400001L);
        //timeSearch.put("value2", 2517629467000L);
        //array.add(timeSearch);


        JSONObject userSearch = new JSONObject();
        userSearch.put("field", "userName");
        userSearch.put("value1", "sc355");
        userSearch.put("asc", true);
        array.add(userSearch);


        List<Article> list = searchAndSortArticles(array, new Timestamp(0), 100, 0);

        for (Article article : list) {
            JSONObject o = article.toJson();
            System.out.println("article:" + o.toJSONString());
        }


    }

    public List<Article> searchAndSortArticles(JSONArray array, Timestamp currentTime, int limit, int offset) {
        List<Condition> ocnditions = new ArrayList<>();
        List<SortField<?>> sorts = new ArrayList<>();

        for (Object o : array) {
            JSONObject jobj = (JSONObject) o;
            String field = (String) jobj.get("field");
            if (field.equals("title")) {
                String value = (String) jobj.get("value1");
                boolean asc = (boolean) jobj.get("asc");
                Condition condition = toCondition("title", value, null);
                if (condition != null) {
                    ocnditions.add(condition);
                }
                SortField<?> sortField = toSortField("title", asc);
                if (sortField != null) {
                    sorts.add(sortField);
                }
            } else if (field.equals("userName")){
                String value = (String) jobj.get("value1");
                boolean asc = (boolean) jobj.get("asc");
                Condition condition = toCondition("userName", value, null);
                if (condition != null) {
                    ocnditions.add(condition);
                }
                SortField<?> sortField = toSortField("title", asc);
                if (sortField != null) {
                    sorts.add(sortField);
                }
            } else if (field.equals("pubtime")) {
                Timestamp value1 = new Timestamp((Long) jobj.get("value1")) ;
                Timestamp value2 = new Timestamp((Long) jobj.get("value2")) ;
                boolean asc = (boolean) jobj.get("asc");
                Condition condition = toCondition("pubtime", value1, value2);
                if (condition != null) {
                    ocnditions.add(condition);
                }
                SortField<?> sortField = toSortField("pubtime", asc);
                if (sortField != null) {
                    sorts.add(sortField);
                }
            }
        }

        if (ocnditions.isEmpty()) {
            return new ArrayList<Article>();
        }

        return searchAndSortArticles(ocnditions, sorts, currentTime, limit, offset);
    }

    public List<Article> searchAndSortArticles(List<Condition> conditions, List<SortField<?>> sorts,
                                               Timestamp currentTime, int limit, int offset) {

        conditions.add(ARTICLE.PUBTIME.le(currentTime));
        conditions.add(ARTICLE.BANNED.eq(0));

        List<Article> list = new ArrayList<>();

        Result<Record> result = create_.select()
                .from(ARTICLE)
                .join(USER).onKey()
                .where(conditions)
                .orderBy(sorts)
                .limit(limit).offset(offset)
                .fetch();

        for (Record r : result) {
            list.add(record2Article(r));
        }

        return list;
    }




    public List<Article> getLastestArticlesOfUser(String userid, int limit, int offset, boolean filterBanned) {
        List<Article> list = new ArrayList<>();

        Condition condition = ARTICLE.USER_ID.eq(Integer.valueOf(userid));
        if (filterBanned) {
            condition = condition.and(ARTICLE.BANNED.eq(0));
        }

        Result<Record> result = create_.select()
                .from(ARTICLE)
                .join(USER).onKey()
                .where(condition)
                .orderBy(ARTICLE.PUBTIME.desc())
                .limit(limit).offset(offset)
                .fetch();
        for (Record r : result) {
            list.add(record2Article(r));
        }

        return list;
    }

    public List<Article> getHotestArticlesOfUser(String userid, int limit, int offset) {
        List<Article> list = new ArrayList<>();

        Result<Record> result = create_.select()
                .from(ARTICLE)
                .join(USER).onKey()
                .where(ARTICLE.USER_ID.eq(Integer.valueOf(userid)))
                .orderBy(ARTICLE.HITCOUNT.desc())
                .limit(limit).offset(offset)
                .fetch();
        for (Record r : result) {
            list.add(record2Article(r));
        }

        return list;
    }

    public Article getArticleById(String article_id) {

        Result<Record> result = create_.select()
                .from(ARTICLE)
                .join(USER).onKey()
                .where(ARTICLE.ID.eq(Integer.valueOf(article_id)))
                .fetch();


        if (result.size() > 0 ) {
            Record r = result.get(0);
            return record2Article(r);
        }


        return null;
    }


    /**
     * search Articles by title pattern
     * @param title
     * @return list of Article whose title contains string 'title'
     */
    public List<Article> getArticlesByTitle(String title) {
        List<Article> list = new ArrayList<>();

        if (title.isEmpty()) {
            return list;
        }

        String pattern = "%" + title + "%";

        Result<Record> result = create_.select()
                .from(ARTICLE)
                .join(USER).onKey()
                .where(ARTICLE.TITLE.like(pattern))
                .fetch();

        for (Record r : result) {
            list.add(record2Article(r));
        }

        return list;
    }

    public void deleteArticleById(String articleid) {

        // delete all comments.
        //deleteCommentByArticleId(articleid);

        create_.delete(ARTICLE)
                .where(ARTICLE.ID.eq(Integer.valueOf(articleid)))
                .execute();
    }

    public void deleteArticleByUserId(String userId) {

        // before delete the articles, we need to delete their comments first.
//        List<Article> list = getAllArticlesOfUser(userId);
//        List<Query> queries = new ArrayList<>();
//
//        for (Article article : list) {
//            queries.add(create_.delete(COMMENT).where(COMMENT.ARTICLE_ID.eq(Integer.valueOf(article.getId()))));
//        }
//
//        create_.batch(queries).execute();

        create_.delete(ARTICLE)
                .where(ARTICLE.USER_ID.eq(Integer.valueOf(userId)))
                .execute();
    }

    public void addArticle(Article article ){

        System.out.println("addArticle time:" + article.getPubtime());

        create_.insertInto(ARTICLE,
                ARTICLE.USER_ID,
                ARTICLE.TITLE,
                ARTICLE.CONTENT,
                ARTICLE.PUBTIME)
                .values(Integer.valueOf(
                        article.getUserId()),
                        article.getTitle(),
                        article.getContent(),
                        article.getPubtime())
                .execute();
    }

    public void modifyAricleContent(String article_id, String title, String content) {
        create_.update(ARTICLE)
                .set(ARTICLE.CONTENT, content)
                .set(ARTICLE.TITLE, title)
                .where(ARTICLE.ID.eq(Integer.valueOf(article_id)))
                .execute();
    }

    public void increaseHitCount(String article_id) throws ArticleNotFoundException {
        Article article = getArticleById(article_id);
        if (article == null) {
            throw new ArticleNotFoundException(article_id);
        }

        create_.update(ARTICLE)
                .set(ARTICLE.HITCOUNT, article.getHitcount() + 1)
                .where(ARTICLE.ID.eq(Integer.valueOf(article_id)))
                .execute();
    }

    public void increaseArticleLikeCount(String article_id) throws ArticleNotFoundException {
        Article article = getArticleById(article_id);
        if (article == null) {
            throw new ArticleNotFoundException(article_id);
        }

        create_.update(ARTICLE)
                .set(ARTICLE.LIKECOUNT, article.getLikecount() + 1)
                .where(ARTICLE.ID.eq(Integer.valueOf(article_id)))
                .execute();
    }

    public void decreaseArticleLikeCount(String article_id) throws ArticleNotFoundException {
        Article article = getArticleById(article_id);
        if (article == null) {
            throw new ArticleNotFoundException(article_id);
        }

        int likes = article.getLikecount();
        likes = likes > 0 ? likes - 1 : 0;

        create_.update(ARTICLE)
                .set(ARTICLE.LIKECOUNT, likes)
                .where(ARTICLE.ID.eq(Integer.valueOf(article_id)))
                .execute();
    }

    public void setArticleBanned(String article_id, boolean banned) {
        create_.update(ARTICLE)
                .set(ARTICLE.BANNED, banned ? 1 : 0)
                .where(ARTICLE.ID.eq(Integer.valueOf(article_id)))
                .execute();
    }

    private Article record2Article(Record r) {
        String id = "" + r.getValue(ARTICLE.ID);
        String userId = "" + r.getValue(ARTICLE.USER_ID);
        String title = r.getValue(ARTICLE.TITLE);
        String content = r.getValue(ARTICLE.CONTENT);

        Timestamp pubtime = r.getValue(ARTICLE.PUBTIME);
        int hitcount = r.getValue(ARTICLE.HITCOUNT);
        int likecount = r.getValue(ARTICLE.LIKECOUNT);
        boolean banned = r.getValue(ARTICLE.BANNED) > 0;

        Article article = new Article(id, userId, title, pubtime, content, hitcount, likecount, banned);

        article.setUserName(r.getValue(USER.USERNAME));
        article.setUserIcon(r.getValue(USER.ICON));
        return article;
    }

    public void addComment(String userId, String articleId, String parentId, String content, Timestamp pubtime) {


        if (parentId != null) {
            create_.insertInto(COMMENT,
                    COMMENT.USER_ID,
                    COMMENT.ARTICLE_ID,
                    COMMENT.PARENT_ID,
                    COMMENT.CONTENT,
                    COMMENT.PUBTIME)
                    .values(Integer.valueOf(userId),
                            Integer.valueOf(articleId),
                            Integer.valueOf(parentId),
                            content,
                            pubtime)
                    .execute();
        } else {
            create_.insertInto(COMMENT,
                    COMMENT.USER_ID,
                    COMMENT.ARTICLE_ID,
                    COMMENT.CONTENT,
                    COMMENT.PUBTIME)
                    .values(Integer.valueOf(userId),
                            Integer.valueOf(articleId),
                            content,
                            pubtime)
                    .execute();
        }

    }

    public void setCommentBanned(String id, boolean banned) {
        create_.update(COMMENT)
                .set(COMMENT.BANNED, banned ? 1 : 0)
                .where(COMMENT.ID.eq(Integer.valueOf(id)))
                .execute();
    }

    public void deleteCommentById(String commentId) {

        System.out.println("delete comment id : " + commentId);

        create_.delete(COMMENT)
                .where(COMMENT.ID.eq(Integer.valueOf(commentId))).execute();

//        create_.batch(
//                create_.delete(COMMENT)
//                        .where(COMMENT.PARENT_ID.eq(Integer.valueOf(commentId))),
//                create_.delete(COMMENT)
//                        .where(COMMENT.ID.eq(Integer.valueOf(commentId))))
//                .execute();
    }

    public void deleteCommentByArticleId(String articleId) {
        create_.delete(COMMENT)
                .where(COMMENT.ARTICLE_ID.eq(Integer.valueOf(articleId)))
                .execute();
    }

    public void deleteCommentByUserId(String userId) {
        create_.delete(COMMENT)
                .where(COMMENT.USER_ID.eq(Integer.valueOf(userId)))
                .execute();
    }

    public void modifyCommentContent(String id, String content) {
        create_.update(COMMENT)
                .set(COMMENT.CONTENT, content)
                .set(COMMENT.PUBTIME, DSL.currentTimestamp())
                .where(COMMENT.ID.eq(Integer.valueOf(id)))
                .execute();
    }


    public List<Comment> getLatestComments(int limit, int offset, boolean filterBanned) {
        List<Comment> comments = new ArrayList<>();

        Result<Record10<Integer, Integer, Integer, Integer, String, Timestamp, Integer, String, String, String>> result = null;

        if (filterBanned) {
            result = create_.select(COMMENT.ID, COMMENT.USER_ID, COMMENT.ARTICLE_ID,
                    COMMENT.PARENT_ID, COMMENT.CONTENT, COMMENT.PUBTIME, COMMENT.BANNED,
                    USER.USERNAME, USER.ICON, ARTICLE.TITLE)
                    .from(COMMENT)
                    .join(USER).onKey()
                    .join(ARTICLE).onKey()
                    .where(COMMENT.BANNED.eq(0))
                    .orderBy(COMMENT.PUBTIME.asc())
                    .fetch();
        } else {
            result = create_.select(COMMENT.ID, COMMENT.USER_ID, COMMENT.ARTICLE_ID,
                    COMMENT.PARENT_ID, COMMENT.CONTENT, COMMENT.PUBTIME, COMMENT.BANNED,
                    USER.USERNAME, USER.ICON, ARTICLE.TITLE)
                    .from(COMMENT)
                    .join(USER).onKey()
                    .join(ARTICLE).onKey()
                    .orderBy( COMMENT.PUBTIME.asc())
                    .fetch();
        }



        for (int i = 0; i < result.size(); i++) {
            Record r = result.get(i);
            Comment comment = record2Comment(r);
            comments.add(comment);
        }

        return comments;
    }


    /**
     *  The comment tree is limited to two level;
     * @param article_id
     * @return
     */
    public List<Comment> getCommentTreeByArticleId(String article_id, boolean filterBanned) {
        List<Comment> topLvComments = new ArrayList<>();

        Condition condition = COMMENT.ARTICLE_ID.eq(Integer.valueOf(article_id));
        if (filterBanned) {
            condition = condition.and(COMMENT.BANNED.eq(0));
        }

        Result<Record9<Integer, Integer, Integer, Integer, String, Timestamp, Integer, String, String>> result
                = create_.select(COMMENT.ID, COMMENT.USER_ID, COMMENT.ARTICLE_ID,
                COMMENT.PARENT_ID, COMMENT.CONTENT, COMMENT.PUBTIME, COMMENT.BANNED,
                USER.USERNAME, USER.ICON)
                .from(COMMENT)
                .join(USER).onKey()
                .where(condition)
                .orderBy(COMMENT.PARENT_ID.asc().nullsFirst(), COMMENT.PUBTIME.asc())
                .fetch();

        System.out.println("comment for " + article_id + ":" + result.size());

        for (int i = 0; i < result.size(); i++) {
            Record r = result.get(i);
            Comment comment = record2Comment(r);
            if (comment.isTopLevelComment()) {
                topLvComments.add(comment);
            } else {
                for (Comment topLvComment : topLvComments) {
                    if (topLvComment.getId().equals(comment.getParentId())) {
                        topLvComment.addChild(comment);
                        break;
                    }
                }
            }
        }

        return topLvComments;
    }

    public List<Comment> getTopLvCommentsByUserId(String user_id, boolean filterBanned) {
        List<Comment> topLvComments = new ArrayList<>();

        Condition condition = COMMENT.USER_ID.eq(Integer.valueOf(user_id)).and(COMMENT.PARENT_ID.isNull());
        if (filterBanned) {
            condition = condition.and(COMMENT.BANNED.eq(0));
        }

        Result<Record8<Integer, Integer, Integer, Integer, String, Timestamp, String, String>> result
                = create_.select(COMMENT.ID, COMMENT.USER_ID, COMMENT.ARTICLE_ID,COMMENT.PARENT_ID, COMMENT.CONTENT, COMMENT.PUBTIME, USER.USERNAME, USER.ICON)
                .from(COMMENT)
                .join(USER).onKey()
                .where(condition)
                .fetch();

        for (Record r : result) {
            Comment comment = record2Comment(r);
            topLvComments.add(comment);
        }

        return topLvComments;
    }

    public List<Comment> getCommentsByParentId(String parent_id) {
        List<Comment> list = new ArrayList<>();

        Result<Record8<Integer, Integer, Integer, Integer, String, Timestamp, String, String>> result
                = create_.select(COMMENT.ID, COMMENT.USER_ID, COMMENT.ARTICLE_ID, COMMENT.PARENT_ID, COMMENT.CONTENT, COMMENT.PUBTIME, USER.USERNAME, USER.ICON)
                .from(COMMENT)
                .join(USER).onKey()
                .where(COMMENT.PARENT_ID.eq(Integer.valueOf(parent_id)))
                .fetch();

        for (Record r : result) {
            list.add(record2Comment(r));
        }

        return list;
    }



    private Comment record2Comment(Record r) {
        String id = "" + r.getValue(COMMENT.ID);
        String userId = "" + r.getValue(COMMENT.USER_ID);
        String article_id = "" + r.getValue(COMMENT.ARTICLE_ID);
        String parent_id =  r.getValue(COMMENT.PARENT_ID) == null ? null : ("" + r.getValue(COMMENT.PARENT_ID));
        String content = r.getValue(COMMENT.CONTENT);
        Timestamp pubtime = r.getValue(COMMENT.PUBTIME);
        String userName = r.getValue(USER.USERNAME);
        String userIcon = r.getValue(USER.ICON);

        System.out.println("id:" + id + ", content:" + content + ", unixtime:" + pubtime.getTime() );

        Comment comment = new Comment(id, userId, article_id, parent_id, content, pubtime, userName, userIcon);
        comment.setBanned(r.getValue(COMMENT.BANNED) != 0);

        return comment;
    }

    private boolean checkValidProperties(Properties properties, String[] mandatoryNames) {
        for (String name : mandatoryNames) {
            String value = properties.getProperty(name);
            if (value == null || value.isEmpty()) {
                return false;
            }
        }

        return true;
    }



    @Override
    public void close() throws Exception {
        if (create_ != null) {
            create_.close();
        }

        if (con_ != null) {
            con_.close();
        }
    }

}
