package ocelot.application;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

class ResetPwdLinkKeeper implements Runnable {

    private Map<String, Reset> resetMap = new HashMap<>();

    @Override
    public void run() {
        checkTime();
    }

    private synchronized void checkTime() {

        System.out.println("CheckTime");

        Iterator<Map.Entry<String, Reset>> iter = resetMap.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry<String, Reset> entry =  iter.next();
            Reset reset = entry.getValue();
            if (reset.isExpired()) {
                iter.remove();
            }
        }
    }

    public synchronized void putReset(String index, Reset reset) {
        resetMap.put(index, reset);
    }

    public synchronized Reset takeReset(String index) {
        return resetMap.remove(index);
    }

    public synchronized boolean hasReset(String index) {
        return resetMap.containsKey(index);
    }
}
