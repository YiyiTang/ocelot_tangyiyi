package ocelot.application.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Logout extends CommonServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

            //invalidate the session if exists
            HttpSession session = req.getSession(false);

            System.out.println("User=" + session.getAttribute("userSession"));

            if (session != null) {
                session.invalidate();
            }
            resp.sendRedirect("Menu.jsp");
        }



    }

