package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.UnsufficientParameterException;
import ocelot.application.beans.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class TestServlet extends CommonServlet {

    String name = "JohnTheDuck";
    String fname = "John";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        try(DBConnection connection = createDBConnection()) {

            out.println("TestAddUser:" + testAddUser(connection));

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean testAddUser(DBConnection connection) throws UnsufficientParameterException {



        List<User> users = connection.getUsersByNamePattern("JohnTheDuck");
        if (users.size() != 1) {
            return false;
        }

        User u = users.get(0);
        if (!u.getFname().equals(fname)){
            return false;
        }
        return true;
    }

    public boolean testArticle(DBConnection connection) throws UnsufficientParameterException {

        return true;

    }

    private void setUp(DBConnection connection) throws UnsufficientParameterException {

        connection.deleteUserByUserName(name);

        User user = new User();

        user.setUserName(name);
        user.setFname(fname);
        user.setLname("Smith");
        user.setPasswd("111111");
        user.setGender("male");
        user.setIconName("default.png");
        user.setEmail("john@gmail.com");
        user.setDob("1901-1-1");
        user.setCountry("UK");
        user.setDescription("xx");

        connection.addUser(user);
    }

    private void shutDown(DBConnection connection) {

        connection.clearDB();
    }



}
