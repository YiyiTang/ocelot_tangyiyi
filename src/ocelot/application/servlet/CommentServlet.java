package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.beans.Article;
import ocelot.application.beans.Comment;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class CommentServlet extends JsonServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        JSONObject jobj = (JSONObject) JSONValue.parse(req.getReader());

        if (jobj == null ) {
            resp.setStatus(400);
            return;
        }

        String method = jobj.get("method").toString();

        switch (method) {

            case "getLatestComments": {
                int limit = Integer.valueOf(jobj.get("limit").toString());
                int offset = Integer.valueOf(jobj.get("offset").toString());
                boolean filterBanned = (Boolean) jobj.get("filter_banned");

                doGetLatestComments(req, resp, limit, offset, filterBanned);
                return;
            }

            case "getCommentTreeByArticleId": {
                String article_id = jobj.get("article_id").toString();
                boolean filterBanned = (Boolean) jobj.get("filter_banned");

                doGetCommentTreeByArticleId(req, resp, article_id, filterBanned);
                return;
            }

            case "addComment": {
                String userId = (String) jobj.get("user_id");
                String article_id = (String) jobj.get("article_id");
                String content = (String) jobj.get("content");
                String parent_id = (String) jobj.get("parent_id");
                Timestamp pubtime = new Timestamp((long)jobj.get("pubtime"));

                doAddComment(req, resp, userId, article_id, parent_id, content, pubtime);
                return;
            }




            case "deleteCommentById": {
                String comment_id = (String) jobj.get("id");

                doDeleteCommentById(req, resp, comment_id);
                return;
            }

            case "setCommentBanned": {
                String comment_id = (String) jobj.get("id");
                boolean banned = (boolean) jobj.get("banned");

                doSetCommentBanned(req, resp, comment_id, banned);
                return;
            }



            default:
                sendError(req, resp, 400,  "No such method " + method + "()");
                break;
        }
    }

    private void doGetLatestComments(HttpServletRequest req, HttpServletResponse resp,
                                             int limit, int offset, boolean filterBanned) throws ServletException, IOException {

        String err = "";

        try(DBConnection connection = createDBConnection()) {

            List<Comment> list = connection.getLatestComments(limit, offset, filterBanned);

            JSONObject jobj = new JSONObject();
            JSONArray commentJsons = new JSONArray();

            for (Comment comment : list) {
                commentJsons.add(comment.toJson());
            }

            System.out.println("getCommentsList size " + list.size());

            jobj.put("result", commentJsons);

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }

    }

    private void doGetCommentTreeByArticleId(HttpServletRequest req, HttpServletResponse resp,
                                           String article_id, boolean filterBanned) throws ServletException, IOException {

        String err = "";

        try(DBConnection connection = createDBConnection()) {

            List<Comment> list = connection.getCommentTreeByArticleId(article_id, filterBanned);

            JSONObject jobj = new JSONObject();
            JSONArray commentJsons = new JSONArray();

            for (Comment comment : list) {
                commentJsons.add(comment.toJson());
            }
            jobj.put("result", commentJsons);

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }

    }

    private void doAddComment(HttpServletRequest req, HttpServletResponse resp,
                              String userId, String article_id, String parent_id, String content, Timestamp pubtime) throws ServletException, IOException {
        String err = "";

        try(DBConnection connection = createDBConnection()) {
            connection.addComment(userId, article_id, parent_id, content, pubtime);

            resp.setStatus(200);
            resp.setContentType("application/json");

            JSONObject jobj = new JSONObject();
            jobj.put("error", null);

            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }


    private void doDeleteCommentById(HttpServletRequest req, HttpServletResponse resp,
                              String comment_id) throws ServletException, IOException {

        try(DBConnection connection = createDBConnection()) {
            connection.deleteCommentById(comment_id);

            resp.setStatus(200);
            resp.setContentType("application/json");

            JSONObject jobj = new JSONObject();
            jobj.put("error", null);

            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }

    private void doSetCommentBanned(HttpServletRequest req, HttpServletResponse resp,
                                     String comment_id, boolean banned) throws ServletException, IOException {

        try(DBConnection connection = createDBConnection()) {
            connection.setCommentBanned(comment_id, banned);

            resp.setStatus(200);
            resp.setContentType("application/json");

            JSONObject jobj = new JSONObject();
            jobj.put("error", null);

            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }
}
