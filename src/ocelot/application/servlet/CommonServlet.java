package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.MailSender;
import ocelot.application.beans.User;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class CommonServlet extends HttpServlet {

    static final String PHOTOS_PATH = "Photos";
    static final String THUMBNAILS = "thumbnails";

    static Properties dbProps = null;
    static Properties mailProps = null;
    private String photoDir;

    static synchronized void loadProperties(HttpServlet servlet) {

        if (dbProps == null) {
            dbProps = new Properties();
            try(FileInputStream fIn = new FileInputStream(servlet.getServletContext().getRealPath("WEB-INF"+ File.separatorChar +"db.properties"))) {
                dbProps.load(fIn);
            } catch (IOException e) {
                e.printStackTrace();
                dbProps = null;
            }
        }

        if (mailProps == null) {
            mailProps = new Properties();
            try(FileInputStream fIn = new FileInputStream(servlet.getServletContext().getRealPath("WEB-INF"+ File.separatorChar +"mail.properties"))) {
                mailProps.load(fIn);
            } catch (IOException e) {
                e.printStackTrace();
                mailProps = null;
            }
        }
    }



    protected String getDefaultThumbNailDir() {
        return photoDir + File.separator + THUMBNAILS;
    }

    protected String getUserPhotoDir(User user) {
        if (user != null) {
            return photoDir + File.separator + user.getId();
        } else {
            return photoDir;
        }
    }

    protected String getUserPhotoDir(String userId) {
        return photoDir + File.separator + userId;
    }

    protected String getRelativePhotoDirUrl () {
        return PHOTOS_PATH;
    }

    protected String getUserRelativeThumbnailDirUrl (User user) {
        if (user != null) {
            return PHOTOS_PATH + "/" + user.getId() + "/" + THUMBNAILS;
        } else {
            return PHOTOS_PATH  + "/" + THUMBNAILS;
        }
    }

    protected String getUserRelativeThumbnailDirUrl (String userId) {
        return PHOTOS_PATH + "/" + userId + "/" + THUMBNAILS;
    }

    protected String getDefaultRelativeThumbnailDirUrl () {
        return PHOTOS_PATH + "/" + THUMBNAILS;
    }

    protected String getUserThumbNailPath(String userId) {
        return getUserPhotoDir(userId) + File.separator + THUMBNAILS;
    }

    protected String getUserThumbNailPath(User user) {
        return getUserPhotoDir(user) + File.separator + THUMBNAILS;
    }

    protected User getUserSession(HttpServletRequest req) {
        return (User) req.getSession().getAttribute("userSession");
    }


    protected List<String> getDefaultAvtarList() {

        String dirName = getDefaultThumbNailDir();

        System.out.println("default thumbnail dir:" + dirName);

        File theDir = new File(dirName);

        String[] l = theDir.list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".png");
            }
        });

        for (int i = 0; i < l.length; i++) {
//            l[i] = "\'" + getDefaultRelativeThumbnailDirUrl() + "/" + l[i] + "\'";
            l[i] = getDefaultRelativeThumbnailDirUrl() + "/" + l[i];

        }

        return new ArrayList<>(Arrays.asList(l));
    }

    protected List<String> getUserAvatarList(String userId) {

        List<String> list = getDefaultAvtarList();

        String dirName = getUserThumbNailPath(userId);

        System.out.println("user " + userId +" thumbnail dir:" + dirName);

        File theDir = new File(dirName);

        if (theDir.exists()) {
            String[] l = theDir.list(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".png");
                }
            });

            for (int i = 0; i < l.length; i++) {
                String url = getUserRelativeThumbnailDirUrl(userId) + "/" + l[i];
                list.add(url);
            }
        }



        return list;
    }

    @Override
    public void init() throws ServletException {
        photoDir = getWebRootPath() + PHOTOS_PATH;
        System.out.println("PhotoDir:" + photoDir);
    }

    public MailSender createMailSender() {
        return MailSender.create(getMailperties());
    }

    protected Properties getDatabaseProperties() {
        if (dbProps == null) {
            loadProperties(this);
        }
        return dbProps;
    }

    protected Properties getMailperties() {
        if (mailProps == null) {
            loadProperties(this);
        }
        return mailProps;
    }

    public DBConnection createDBConnection() throws SQLException {
        return DBConnection.create(getDatabaseProperties());
    }

    public String getWebRootPath() {
        return getServletContext().getRealPath("/");
    }


    public void sendErrorPage(HttpServletRequest req, HttpServletResponse resp, int status, String content) throws IOException {

        PrintWriter out = resp.getWriter();

        resp.setStatus(status);
        out.println("<html>\n<head><title>Send Email Succeed</title></head>\n<body>");
        out.println(content);
        out.println("</body></html>");
    }

    public void showError(HttpServletRequest req, HttpServletResponse resp, int status, String content) throws IOException {

        String err = "<h2>" + content + "</h2>";
        sendErrorPage(req, resp, status, err);
    }

    public void sendJsonError(HttpServletRequest req, HttpServletResponse resp, int status, String content) throws IOException {

        PrintWriter out = resp.getWriter();

        resp.setContentType("application/json");

        resp.setStatus(status);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error", content);
        out.println(jsonObject.toJSONString());
    }

}
