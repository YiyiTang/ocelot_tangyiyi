package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.beans.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UpdateUserSessionServlet extends JsonServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JSONObject jobj = (JSONObject) JSONValue.parse(req.getReader());

        if (jobj == null ) {
            sendError(req, resp, 400, "invalid input parameter");
            return;
        }

        String err = "";

        String method = jobj.get("method").toString();

        if (method.equals("updateUserSession")) {
            try(DBConnection connection = createDBConnection()) {
                User user = (User) req.getSession().getAttribute("userSession");
                if (user != null) {
                    user = connection.getUserById(user.getId());
                    if (user != null) {
                        req.getSession().setAttribute("userSession", user);

                        resp.setStatus(200);
                        resp.setContentType("application/json");

                        JSONObject obj = new JSONObject();

                        resp.getWriter().println(obj.toJSONString());
                        return;
                    }
                } else {
                    err = "user outdated";
                }

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
