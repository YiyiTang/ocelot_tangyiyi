package ocelot.application.servlet;

import ocelot.application.beans.User;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

public abstract class UploadServlet extends CommonServlet {

    private int maxFileSize = 500 * 1024 * 1024;
    private int maxMemSize = 128 * 1024;


    /**
     *
     * @param user
     * @param fi
     * @return  relative url path to saved file
     */
    protected abstract String onUploadFile(User user, FileItem fi);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        boolean isMultipart = ServletFileUpload.isMultipartContent(req);
        resp.setContentType("application/json");

        PrintWriter out = resp.getWriter();

        if ( !isMultipart) {
            sendJsonError(req, resp, 400, "No file to be uploaded!");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

        try {
            List<FileItem> fileItems  = upload.parseRequest(req);

            Iterator iter = fileItems.iterator();

            JSONObject jobj = new JSONObject();
            JSONArray array = new JSONArray();

            while (iter.hasNext()) {

                FileItem fi = (FileItem)iter.next();
                if ( !fi.isFormField () ) {
                    // Get the uploaded file parameters



                    User user = getUserSession(req);

                    String name = onUploadFile(user, fi);

                    if (name != null) {
                        JSONObject f = new JSONObject();
                        f.put("url", name);
                        array.add(f);
                    }
                }
            }

            jobj.put("result", array);

            resp.setStatus(200);
            resp.setContentType("application/json");
            out.println(jobj.toJSONString());


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
