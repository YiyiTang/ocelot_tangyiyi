Agenda
=======
1. To make navigation bar for all websites responsive.
2. To make all websites layout responsive.
3. To upload our work to server.

Minutes/Notes
=======
1.Successfully make navigation bar for all websites responsive 
- Every group members involved in the designing and coding of making website navigation bar responsive.

2.Successfully unified the layout of all websites
-  group members involved in the unifying of layout of all websites

3.Sucessfully upload our work to server
- Every group members involved in the process and coding of uploading our work to server

Next Steps:
=======
1. To make our website responsive. 
2. To search for bug in our websites.
