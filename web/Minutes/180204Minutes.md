Agenda
=======
1.Modify bug for summernote
2.Implement load more article function for main page

Minutes/Notes
=======
1.Successfully rectify bug for summernote
- Every group members involved in rectifying bug for summernote

2.Successfully implement load more article function for main page
- Every group members involved in coding and design of load more article function for main page

Next Steps:
=======
1. To complete search function for main page
2. To perfect the design and layout of the website
