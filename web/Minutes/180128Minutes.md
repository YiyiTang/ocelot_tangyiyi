Agenda
=======
1. To update login & registration page layout
2. To design and create "Create Article" layout
3. to implement Google reCaptcha verify function in server side (Servlet)


Minutes/Notes
=======
1.Successfully updated login & registration page layout
- Every group member involved in the layout updated and bugs fix.

2.Successfully designed and completed create article page layout
- Every group member involved in the process of design and coding of create article page layout.

3. Successfully implemented Google reCaptcha verify function in server side
- Every group member involved in coding of Google reCaptcha servlet.

4. Database structure updated
- Tang involved in design and database update.


Next Steps:
=======
1. To retrieve article from database for personal page. 
2. To successfully implement log out function for personal page. 
3. To create article and save into database.
4. To update registration form input
