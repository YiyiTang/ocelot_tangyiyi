Agenda
=======
1.	Project management
2.	Bitbucket – Git repository setup 
3.	Project configuration setup
4.	Database design & implementation 
5.	Web structure design & implementation

Minutes/Notes
=======
1.Project management (All participants agreed)
- Daily meeting time aligned
          -  Morning briefing - from 9:30am to 10:00am 
           - Evening meeting – from 5:30pm to 6:00pm
- Communication tools aligned

2.Bitbucket – Git repository setup (All participants involved)
- Master repository & team members repository creation

3.Project setup – (All participants involved)
- Using intelliJ as our IDE
- Servlet configuration & deployment

4.Database design & implementation
- Discussion of database design  (All participants involved)
- Database design with ER Diagram (Yiyi Tang)
- Database creation & implementation (Yiyi Tang)

5.Web structure design & implementation
- Discussion of main web structure design – All participants involved
- Main page design & implementation – Mark Lee
- Login page design & implementation – Nelson Chan

Next Steps:
=======

1.Completion of main page design (All participants involved)
-Discussion of main page design and improvement ( All participants involved)

2.Compilation of main page and log in page(All participants involved)
-Every group member involved in compilation of main page and log in page
-reCAPTCHA design completed (Nelson)

3.Completion of personal page design 
- Every group member involved in compilation of personal page design 
- WYSIWYG being created and discussed among members 

4.Design and coding of servlet classes 
- All group members are involved in designing and coding of servlet 




