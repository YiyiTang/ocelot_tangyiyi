<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="js/ajax/User.js"></script>

    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>

    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>


    <style>
        img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }


    </style>
</head>
<body>


<div class="container-fluid">
    <!--<div class="picker" hidden>
        <select class="image-picker show-html">
            <option value=""></option>
            <option data-img-src="http://placekitten.com/300/200" value="1">Cute Kitten 1</option>
            <option data-img-src="http://placekitten.com/150/200" value="2">Cute Kitten 2</option>
            <option data-img-src="http://placekitten.com/400/200" value="3">Cute Kitten 3</option>
        </select>
    </div>-->
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div id="Carousel" class="carousel slide">

                <ol class="carousel-indicators">
                    <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                </ol>

                <!-- Carousel items -->
                <div class="carousel-inner">

                    <div class="item active">
                        <div id="avatar" class="rows">



                        </div><!--.row-->
                    </div><!--.item-->


                    <form id="file-form" method="POST" action="UploadThumbnailServlet" >
                        <div>
                            <input type="file" id="file-select" name="photos[]" multiple/>
                            <button type="submit" id="upload-button">Upload</button>
                        </div>
                    </form>

                </div><!--.carousel-inner-->
            </div><!--.Carousel-->

        </div>
    </div>
</div><!--.container-->



<script>

    var userId = null;

    function loadAvatarList() {

        <c:if test="${not empty userSession}">
            userId = "${userSession.id}";
        </c:if>

        getAvatarList(userId,

            function (data) {
                var list = data.result;

                for (var i = 0 ; i < list.length; i++) {
                    addImage(list[i]);
                }
                <c:if test="${not empty userSession}">
                    setSelectedAvatar("${userSession.iconName}");
                </c:if>
            }
        );
    }


    function addImage(img_url) {

        var select = $("#avatar");


        var div = $("<div></div>");
        div.attr("class", "col-sm-3 col-xs-6");
        var img = $("<img>");
        img.attr("src", img_url);

        div.append(img);

        img.on("click", function () {
            var picker = $("#avatar");

            picker.attr("value", $(this).attr("src"));
            parent.setIcon($(this).attr("src"));
            clearImageBgColor();
            $(this).parent().css("background-color", "#dad7e5");
        })

        select.append(div);

    }

    function clearImageBgColor() {
        $("#avatar div").css("background-color", "white");
    }

    function initializeImagePicker() {

        var img = $("#avatar > div > img").each(function () {


            $(this).on("click", function () {

                //console.log("src:" +  $(this).attr("src"));
                var picker = $("#avatar");

                picker.attr("value", $(this).attr("src"));
                parent.setIcon($(this).attr("src"));
                clearImageBgColor();
                $(this).parent().css("background-color", "#dad7e5");

            });

        });

        var firstImg = $("#avatar > div > img").first();
        $("#avatar").attr("value", firstImg.attr("src"));

    }


    function setSelectedAvatar(img) {

        var imgs = $("#avatar > div > img");

        for (var i = 0; i < imgs.length; i++) {
            var content = $(imgs[i]).attr("src");
            if (content == img) {
                clearImageBgColor();
                $(imgs[i]).parent().css("background-color", "#dad7e5");
                parent.setIcon(getIcon());
            }
        }

    }

    function setupUpload(form, fileSelect, uploadButton, success, error) {
        form.onsubmit = function(event) {
            event.preventDefault();

            // Update button text.
            uploadButton.innerHTML = 'Uploading...';

            // The rest of the code will go here...

            // Get the selected files from the input.
            var files = fileSelect.files;

            // Create a new FormData object.
            var formData = new FormData();

            // Loop through each of the selected files.
            for (var i = 0; i < files.length; i++) {
                var file = files[i];

                // Check the file type.
                if (file.type.match('image.*')) {

                    // Add the file to the request.
                    var name = fileSelect.getAttribute("name");
                    formData.append(name, file, file.name);

                } else {
                    continue;
                }
            }

            var xhr = new XMLHttpRequest();

            var url = form.getAttribute("action");

            // Open the connection.
            xhr.open('POST', url, true);

            // Set up a handler for when the request finishes.
            xhr.onload = function () {
                if (xhr.status === 200) {
                    // File(s) uploaded.
                    uploadButton.innerHTML = 'Upload';
                    success(JSON.parse(xhr.responseText));
                } else {
                    alertify.alert('An error occurred!', function () {
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                    error(xhr.status, xhr.responseText);
                }
            };

            // Send the Data.
            xhr.send(formData);

        }
    }


    function getIcon() {
        return $("#avatar").attr("value");
    }

    $(function () {

        loadAvatarList();
        initializeImagePicker();

        var form = document.getElementById('file-form');
        var fileSelect = document.getElementById('file-select');
        var uploadButton = document.getElementById('upload-button');

        setupUpload(form, fileSelect, uploadButton,
            function (data) {
                var list = data["result"];
                for (var i = 0; i < list.length; ++i) {
                    console.log("url:" + list[i].url);
                    addImage(list[i].url);
                }
                alertify.alert("upload success!" + data, function () {
                    alertify.message('OK');
                }).setHeader('<em> Team Ocelot </em> ');
            },

            function (statuscode, data) {
                alertify.alert("" + statuscode + " " + data, function () {
                    alertify.message('OK');
                }).setHeader('<em> Team Ocelot </em> ');
            }
        );

    });

</script>




</body>
</html>