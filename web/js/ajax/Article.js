

// $(function () {
//     $.getScript( "Helper.js", function( data, textStatus, jqxhr ) {
//         console.log( data ); // Data returned
//         console.log( textStatus ); // Success
//         console.log( jqxhr.status ); // 200
//         console.log( "Load was performed." );
//     });
// });

function getLastestArticles(limit, offset, currentTime, success, error, filter_banned) {


    var obj = {
        "method": "getLastestArticles",
        "limit" : limit,
        "offset" : offset,
        "currentTime" : currentTime
    };

    obj.filter_banned = true;
    if (typeof filter_banned === 'boolean' ) {
        obj.filter_banned = filter_banned;
    }

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function getArticleById(id, success, error) {


    var obj = {
        "method": "getArticleById",
        "id" : id,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function getLastestArticlesOfUser(user_id, limit, offset, success, error, filter_banned) {

    var obj = {
        "method": "getLastestArticlesOfUser",
        "user_id": user_id,
        "limit" : limit,
        "offset" : offset
    };

    obj.filter_banned = true;
    if (typeof filter_banned === 'boolean' ) {
        obj.filter_banned = filter_banned;
    }

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function addArticle(user_id, title, content, success, error, pubtime) {
    var date = null;
    if (pubtime == null
        || pubtime == undefined) {
        date = currentUnixTime();
    } else {
        date = pubtime;
    }

    console.log("pubtime:" + date);

    var obj = {
        "method": "addArticle",
        "user_id": user_id,
        "title": title,
        "content": content,
        "pubtime": date
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}


function deleteArticleById(id, success, error) {

    var obj = {
        "method": "deleteArticleById",
        "id": id,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function modifyArticleById(id, title, content, success, error) {

    var obj = {
        "method": "modifyArticleById",
        "id": id,
        "title": title,
        "content": content,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function setArticleBanned(id, banned, success, error) {

    var obj = {
        "method": "setArticleBanned",
        "id": id,
        "banned":banned,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}


function searchArticles(limit, offset, rules, currentTime, success, error) {


    var obj = {
        "method": "searchArticles",
        "limit" : limit,
        "offset" : offset,
        "rules" : rules,
        "currentTime" : currentTime
    };


    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'ArticleServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}