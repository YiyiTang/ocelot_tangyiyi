
// $(function () {
//     $.getScript( "Helper.js", function( data, textStatus, jqxhr ) {
//         console.log( data ); // Data returned
//         console.log( textStatus ); // Success
//         console.log( jqxhr.status ); // 200
//         console.log( "Load was performed." );
//     });
// });


function getLatestComments(limit, offset, success, error, filter_banned) {

    var obj = {
        "method": "getLatestComments",
        "limit": limit,
        "offset": offset
    };

    obj.filter_banned = false;
    if (typeof filter_banned === 'boolean' ) {
        obj.filter_banned = filter_banned;
    }

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'CommentServlet',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function getCommentTreeByArticleId(article_id, success, error, filter_banned) {

    var obj = {
        "method": "getCommentTreeByArticleId",
        "article_id": article_id,
    };

    obj.filter_banned = true;

    if (typeof filter_banned === 'boolean' ) {
        obj.filter_banned = filter_banned;
    }

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'CommentServlet',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function addComment(user_id, article_id, parent_id, content, success, error) {
    var time = currentUnixTime();


    var obj = {
        "method": "addComment",
        "user_id": user_id,
        "article_id": article_id,
        "parent_id": parent_id,
        "content": content,
        "pubtime": time,
    };


    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'CommentServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}

function deleteCommentById(id, success, error) {

    var obj = {
        "method": "deleteCommentById",
        "id": id,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'CommentServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}


function setCommentBanned(id, banned, success, error) {

    var obj = {
        "method": "setCommentBanned",
        "id": id,
        "banned":banned,
    };

    $.ajax
    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: 'CommentServlet',
        dataType: 'json',
        data: JSON.stringify(obj),
        async: true,
        success: success,
        statusCode: {
            400: error
        }
    });
}