<%--
  Created by IntelliJ IDEA.
  User: yt172
  Date: 30/01/2018
  Time: 1:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>

    <title>ResetPassword</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>

    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>

    <script src="js/Helper.js"></script>
    <script src="js/ajax/User.js"></script>
    <title>ResetPassword</title>

</head>
<body>
<div>

    <c:choose>
        <c:when test="${sessionScope.userSession != null}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="text-center">Hi ${userSession.fname}, please change your password</h2>
                </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="text-center">
                                        <div id="editForm" action="UserServlet" method="post">
                                            <label>New password:</label>
                                            <input type="password" id="new_pwd1"><br>
                                            <label>Confirm password:</label>
                                            <input type="password" id="new_pwd2"><br>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <button type="button" id="summit">Confirm</button>
                        <input id="reset" type="reset" value="Reset"><br>
                        <p id="err"></p>
                    </div>
                </div>
            </div>
        </div>
</div>
            <script>

                function clear() {
                    $("#new_pwd1").val("");
                    $("#new_pwd2").val("");
                    $("#summit").prop("disabled", true);
                    $("#err").text("");
                }

                function checkButtonState() {
                    var new_value1 = $("#new_pwd1").val();
                    var new_value2 = $("#new_pwd2").val();

                    if ((new_value1 !== new_value2)) {
                        $("#err").text("The two entered new password are not the same!");
                    } else {
                        $("#err").text("");
                    }

                    if (new_value1 === new_value2) {
                        $("#summit").prop("disabled", false);
                    } else {

                    }
                }

                $(function () {
                    clear();
                    $("#new_pwd1").on("change", checkButtonState);
                    $("#new_pwd2").on("change", checkButtonState);

                    $("#reset").click(clear);

                    $("#summit").click(function () {

                        var new_value2 = $("#new_pwd2").val();
                        var new_value1 = $("#new_pwd1").val();

                        if (new_value1.length === 0) {
                            alertify.alert("password cannot be empty!");
                        }

                        if (new_value1 !== new_value2){
                            alertify.alert("two password unmatched");
                            return;
                        }

                        resetUserPasswd("${userSession.id}", new_value1,

                            function (data) {
                                alertify.alert('Change password succeed', function(){ window.location.replace("Menu.jsp");}).setHeader('<em> Team Ocelot </em> ');
                                console.log("change password succeed");

                            },

                            function (data) {
                                alertify.alert("change password failed " + JSON.stringify(data));
                                console.log("\"change password failed \" + JSON.stringify(data)");
                            }
                        );

                    });
                })

            </script>


        </c:when>
        <c:otherwise>
            <header>
                    <h1>The session is expired, please ask your administrator to send your another email.</h1>
            </header>


        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
